# Unity Gridmap

Unity Gridmap is my attempt to become more familiar with the Unity engine tooling and packaging systems, and is a work in progress.

The tool is inspired by the Godot Gridmap tool, which you can find more information about here: https://docs.godotengine.org/en/3.1/tutorials/3d/using_gridmaps.html

## Installation
### Unity package
- Download the UnityGridmap.unitypackage at the root of this repository to a known location.
- Open or create a Unity project that you would like to use the tool in. At the top of the Unity editor window, navigate to Assets > Import Package > Custom Package.
- Select the downloaded .unitypackage file, leave all ticked values as default, and click Import.

### Example project
This repository also features an example project, which contains:
- A pre-installed version of the Unity package
- An example scene at UnityGridmap\Assets\Scenes
- A number of pre-configured prefab objects to paint. These are taken from the 3D Road Tiles pack by the wonderful Kenney: https://kenney.nl/assets/3d-road-tiles

## Usage
### Enabling the tool
Once the tool is installed, this can be done by selecting the Available Custom Editor Tools button in the top left hand corner of the editor window (the icon with a crossed pencil and wrench) and choosing Unity Gridmap

### Constraints
Please note that the following is required for the tool to work correctly:
- Each game object to be used as a tile must be saved as a prefab and contain at least one collider that derives from the Unity Collider class
- For saving tiles to work correctly, each prefab must be located directly under a folder named Resources
- The editor window must retain focus for painting to work, so you may need to left click on the screen to regain focus if painting does not work

### Interaction
- Use the left mouse button to select options in the left-hand settings panel and move the selected tile around the grid
- A to paint the current tile on to the grid
- S to remove the grid tile currently being hovered over by the mouse cursor
- D button to rotate the tile 90 degrees counter-clockwise

### Settings
- Cell Size: The X/Z plane size of each cell on the grid
- Grid Scale: The number of cells between the center and edge of the grid. For example, a value of 7 would return a 14 x 14 cell grid
- Grid Origin: The center location of the grid
