﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.EditorTools;
using UnityEditorInternal;
using UnityEngine;

/// <summary> Represents the profile for a tile that can be painted on to the grid </summary>
public struct TileInfo
{
    public GameObject TilePrefab { set; get; }
    public GUIContent TileGUIContent { set; get; }
    public string TilePath { set; get; }
}

[EditorTool("Unity Gridmap")]
public class UnityGridmap : EditorTool {

    // Grid information
    private float CellSize { set; get; }
    private int GridScale { set; get; }
    private Vector3 GridOrigin { set; get; }
    private Vector3 oldGridOrigin;
    private float GridSize { set; get; }
    private Plane gridPlane;
    private readonly float MinCellSize = 1.0f;
    private readonly float MaxCellSize = 256.0f;
    private readonly int MinGridScale = 1;
    private readonly int MaxGridScale = 1000;
    private readonly Color GridLineColor = new Color(1.0f, 0.3f, 0.3f, 0.2f);

    // Tile information
    [SerializeField]
    private Dictionary<string, TileInfo> tilePalette = null;
    private int currentTileIndex = 0;
    private int prevTileIndex = -1;

    // Cursor information
    [HideInInspector]
    public GameObject cursorTile;
    public GameObject cursorTileChild;
    private Vector2 scrollPosition = Vector2.zero;
    private Vector2 cursorScreenPos;
    private Vector3 cursorWorldPos;
    private Vector3 selectedCellPos;

    // Icon information
    [SerializeField]
    Texture2D m_ToolIcon;
    GUIContent m_IconContent;

    // Settings information
    private UnityGridmapSettings gridSettings;
    private readonly string SettingsFilePath = "Assets/Editor/UnityGridmap/Settings.asset";
    private SerializedObject gridSettingsSerial;
    private bool ShouldShowPanel { set; get; } = true;
    private readonly float MinPanelWidth = 275.0f;

    /// <summary> Loads any saved settings values into the tool, or creates a new settings instance with default values if no saved version exists </summary>
    public void CreateOrLoadSettings()
    {
        LogGridmap("Running load settings");

        gridSettings = AssetDatabase.LoadAssetAtPath<UnityGridmapSettings>(SettingsFilePath);
        if (gridSettings == null)
        {
            gridSettings = CreateInstance<UnityGridmapSettings>();
            AssetDatabase.CreateAsset(gridSettings, SettingsFilePath);
            AssetDatabase.SaveAssets();
        }

        gridSettingsSerial = new SerializedObject(gridSettings);
        gridSettingsSerial.FindProperty("cellSize").floatValue = gridSettings.cellSize;
        gridSettingsSerial.FindProperty("gridScale").intValue = gridSettings.gridScale;
        gridSettingsSerial.FindProperty("gridOrigin").vector3Value = gridSettings.gridOrigin;

        // Create a new tilePalette dict if an existing one is not present
        if (tilePalette == null)
        {
            tilePalette = new Dictionary<string, TileInfo>();
        }

        LogGridmap("Attempting to retrieve saved tile paths");
        for (int i = 0; i < gridSettings.tilePaths.Count; ++i)
        {
            GameObject tile = Resources.Load(gridSettings.tilePaths[i], typeof(GameObject)) as GameObject;
            // If the tile loads correctly and is not already contained in the tile palette
            if (tile != null && !tilePalette.ContainsKey(tile.name))
            {
                // Add the tile to the tile palette
                tilePalette.Add(
                    tile.name,
                    GenerateNewTileInfo(tile)
                );
            }
        }

        gridSettingsSerial.ApplyModifiedProperties();
    }

    /// <summary> Updates the grid settings currently saved on disk </summary>
    public void SaveSettings()
    {
        // Update the grid settings serial values to match the current GUI values
        gridSettingsSerial.FindProperty("cellSize").floatValue = CellSize;
        gridSettingsSerial.FindProperty("gridScale").intValue = GridScale;
        gridSettingsSerial.FindProperty("gridOrigin").vector3Value = GridOrigin;

        // Record the changes to the serial object in preparation for saving
        gridSettingsSerial.ApplyModifiedProperties();

        // Write the changes to disk
        EditorUtility.SetDirty(gridSettings);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        LogGridmap("Save operation complete");
    }

    /// <summary> Processes relevant behaviour when the tool is enabled </summary>
    public void OnEnable()
    {
        // Set up the tool's icon information
        m_IconContent = new GUIContent()
        {
            image = m_ToolIcon,
            text = "Unity Gridmap",
            tooltip = "Unity Gridmap"
        };

        // Register the HandleToolChange method to be called whenever Unity's active tool changes
        EditorTools.activeToolChanged += HandleToolChange;

        RegisterNewTag("GridmapTile");

        CreateOrLoadSettings();

        LogGridmap("Cell size after loading: " + gridSettings.cellSize);

        GridSize = gridSettings.cellSize * gridSettings.gridScale;

        gridPlane = new Plane(Vector3.up, GridOrigin);

        CreateNewCursorTile();
    }

    public void OnDestroy()
    {
        DestroyImmediate(cursorTileChild);
        cursorTileChild = null;

        DestroyImmediate(cursorTile);
        cursorTile = null;
    }

    /// <summary> Attempts to register a given tag name in Unity's tag manager </summary>
    /// <param name="_newTagName">The name of the tag to register</param>
    private void RegisterNewTag(string _newTagName)
    {
        LogGridmap("Attempting to register tag: " + _newTagName);

        // Load the tags property
        SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
        SerializedProperty tagsProperty = tagManager.FindProperty("tags");

        // Check to see if the tag already exists
        bool tagExists = false;
        foreach (SerializedProperty tag in tagsProperty)
        {
            if (tag.stringValue.Equals(_newTagName))
            {
                tagExists = true;
                LogGridmap("Tag has already been registered");
                break;
            }
        }

        // If the tag does not already exist, register it
        if (!tagExists)
        {
            tagsProperty.InsertArrayElementAtIndex(0);
            SerializedProperty newTag = tagsProperty.GetArrayElementAtIndex(0);
            newTag.stringValue = _newTagName;

            tagManager.ApplyModifiedProperties();

            LogGridmap("New tag has been registered: " + tagsProperty.GetArrayElementAtIndex(0).stringValue);
        }
    }

    /// <summary> Processes relevant behaviour when the tool is either selected or deselected </summary>
    private void HandleToolChange()
    {
        // If the tool is deselected
        if (!EditorTools.IsActiveTool(this))
        {
            LogGridmap("Tool Deseleced");
            if (cursorTile != null)
            {
                DestroyImmediate(cursorTileChild);
                cursorTileChild = null;

                DestroyImmediate(cursorTile);
                cursorTile = null;
            }
        }
        // If the tool is selected
        else
        {
            LogGridmap("Tool Selected");
            if (tilePalette != null && tilePalette.Count > 0)
            {
                CreateNewCursorTile();
            }
        }
    }

    /// <summary> Returns the toolbar icon to be displayed in the editor's top left tool selection panel </summary>
    public override GUIContent toolbarIcon
    {
        get { return m_IconContent; }
    }

    /// <summary> Updates the tool GUI on each frame that the tool is enabled </summary>
    /// <param name="_window">A reference to the editor window</param>
    public override void OnToolGUI(EditorWindow _window)
    {
        base.OnToolGUI(_window);

        float panelWidth = Mathf.Max(Camera.current.pixelWidth * 0.2f, MinPanelWidth);

        if (gridSettingsSerial == null)
        {
            CreateOrLoadSettings();
        }

        CellSize = gridSettingsSerial.FindProperty("cellSize").floatValue;
        GridScale = gridSettingsSerial.FindProperty("gridScale").intValue;

        oldGridOrigin = GridOrigin;
        GridOrigin = gridSettingsSerial.FindProperty("gridOrigin").vector3Value;

        ProcessGridOriginChange();

        // Check to see if any items in the GUI have been changed in the last frame
        EditorGUI.BeginChangeCheck();

        Event currentEvent = Event.current;

        // If the mouse cursor has been moved again since the last movement
        if (currentEvent.mousePosition != cursorScreenPos)
        {
            // Calculate and record both the screen and world positions of the cursor
            cursorScreenPos = currentEvent.mousePosition;
            UpdateCursorWorldPos(currentEvent);

            // Adjust the position of the cursor so that it sits inside the nearest grid cell
            SnapTileCursorToGridCell();
        }

        // If the user presses or holds the tile placement key
        if (currentEvent.keyCode == KeyCode.A)
        {
            RaycastHit hitInfo = new RaycastHit();
            CheckForRayTileCollision(ref currentEvent, ref hitInfo);

            if (hitInfo.collider == null || hitInfo.collider == cursorTile.GetComponentInChildren<Collider>() || hitInfo.collider.tag != "GridmapTile")
            {
                AttemptTilePlacement();
            }
        }
        // If the user presses or holds the tile deletion key
        else if (currentEvent.keyCode == KeyCode.S)
        {
            AttemptTileDeletion(currentEvent);
        }

        ProcessTileRotation(ref currentEvent);

        DrawGridLines();

        /*********************************
            ON-SCREEN GUI
        *********************************/
        Handles.BeginGUI();

        Rect panelRect = new Rect(10.0f, 10.0f, panelWidth, Camera.current.pixelHeight);
        float panelControlWidth = panelWidth * 0.9f;

        GUILayout.BeginArea(panelRect);

        if (ShouldShowPanel)
        {
            CellSize = EditorGUILayout.Slider("Cell Size", CellSize, MinCellSize, MaxCellSize, GUILayout.Width(panelControlWidth));
            GridScale = (int)EditorGUILayout.Slider("Grid Scale", GridScale, MinGridScale, MaxGridScale, GUILayout.Width(panelControlWidth));

            oldGridOrigin = GridOrigin;
            GridOrigin = EditorGUILayout.Vector3Field("Grid Origin", GridOrigin, GUILayout.Width(panelControlWidth));
            ProcessGridOriginChange();

            gridSettingsSerial.FindProperty("cellSize").floatValue = CellSize;
            gridSettingsSerial.FindProperty("gridScale").intValue = GridScale;
            gridSettingsSerial.FindProperty("gridOrigin").vector3Value = GridOrigin;
        }

        GridSize = CellSize * GridScale;

        if (Selection.gameObjects.Length > 0 && ShouldShowPanel)
        {
            string addTileButtonLabel = "";
            if (Selection.gameObjects.Length > 1)
            {
                addTileButtonLabel = "Add Selected Objects as Tile";
            }
            else
            {
                addTileButtonLabel = "Add Selected Object as Tile";
            }

            if (GUILayout.Button(addTileButtonLabel, GUILayout.Width(panelControlWidth)))
            {
                AddTilesToPalette();
            }
        }
        else if (tilePalette != null && tilePalette.Count <= 0 && ShouldShowPanel)
        {
            GUILayout.Label("No tiles loaded. Select one or more to begin.");
        }

        if (tilePalette != null && tilePalette.Count > 0 && ShouldShowPanel)
        {
            if (GUILayout.Button("Remove Selected Tile", GUILayout.Width(panelControlWidth)))
            {
                RemoveSelectedTileFromPalette();
            }
        }

        if ((tilePalette != null) && (tilePalette.Count > 0) && ShouldShowPanel)
        {
            List<GUIContent> tileGUIContent = new List<GUIContent>();
            foreach (KeyValuePair<string, TileInfo> tile in tilePalette)
            {
                tileGUIContent.Add(tile.Value.TileGUIContent);
            }

            scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(_window.position.width * 0.15f), GUILayout.Height(_window.position.height * 0.7f));
            prevTileIndex = currentTileIndex;

            currentTileIndex = GUILayout.SelectionGrid(
                currentTileIndex,
                tileGUIContent.ToArray(),
                1,
                GUILayout.Width(_window.position.width * 0.1f),
                GUILayout.Height(_window.position.width * 0.1f * tilePalette.Count)
            );

            GUILayout.EndScrollView();

            // If the currently selected tile has been changed or no cursor tile is present
            if (prevTileIndex != currentTileIndex || cursorTile == null)
            {
                // Destroy any existing cursorTile instance and create a new one
                DestroyImmediate(cursorTile);
                CreateNewCursorTile();
            }
        }
        else if (cursorTile != null)
        {
            DestroyImmediate(cursorTile);
        }

        if (ShouldShowPanel)
        {
            if (GUILayout.Button("Save Settings", GUILayout.Width(panelControlWidth)))
            {
                SaveSettings();
            }
        }

        string showPanelText = "";
        if (ShouldShowPanel)
        {
            showPanelText = "Hide Panel";
        }
        else
        {
            showPanelText = "Show Panel";
        }

        // If the show panel is clicked, change from shown to hidden or vice vers
        if (GUILayout.Button(showPanelText, GUILayout.Width(panelControlWidth)))
        {
            ShouldShowPanel = !ShouldShowPanel;
        }

        GUILayout.EndArea();

        Handles.EndGUI();

        EditorGUI.EndChangeCheck();
    }

    private void ProcessGridOriginChange()
    {
        // If the grid origin has changed since the last GUI update
        if (GridOrigin != oldGridOrigin)
        {
            // Create a new grid plane at the new grid origin
            gridPlane = new Plane(Vector3.up, GridOrigin);
        }
    }

    /// <summary> Checks if the user has pressed either rotation key in the current event and processes tile rotation accordingly </summary>
    /// <param name="_currentEvent">A reference to the current event</param>
    private void ProcessTileRotation(ref Event _currentEvent)
    {
        if (cursorTile != null)
        {
            float rotationModifier = 0.0f;

            if (_currentEvent.keyCode == KeyCode.D && _currentEvent.rawType == EventType.KeyDown)
            {
                rotationModifier = -90.0f;
            }

            cursorTileChild.transform.localPosition = new Vector3(0.5f, 0.0f, 0.5f);
            cursorTile.transform.Rotate(Vector3.up, rotationModifier, Space.World);
        }
    }

    /// <summary> Draws a series of lines on the X and Z axes representing the tile placement grid</summary>
    private void DrawGridLines()
    {
        // Set the grid color to a non-white color to help distinguish it from everything else
        Handles.color = GridLineColor;

        // Draw a gridSize + cellSize (the addition is to ensure that a border is drawn on the far end)
        // number of grid lines perpendicular to the world Z-axis at cellSize increments and at the gridOrigin height
        for (float z = GridOrigin.z - GridSize - (CellSize * 0.5f); z < GridOrigin.z + GridSize; z += CellSize)
        {
            Handles.DrawLine(
                new Vector3(GridOrigin.x - GridSize + CellSize, GridOrigin.y, Mathf.Ceil(z / CellSize) * CellSize + CellSize),
                new Vector3(GridOrigin.x + GridSize + CellSize, GridOrigin.y, Mathf.Ceil(z / CellSize) * CellSize + CellSize)
            );
        }

        // Draw a gridSize + cellSize (the addition is to ensure that a border is drawn on the far end)
        // number of grid lines perpendicular to the world X-axis at cellSize increments and at the gridOrigin height
        for (float x = GridOrigin.x - GridSize - (CellSize * 0.5f); x < GridOrigin.x + GridSize; x += CellSize)
        {
            Handles.DrawLine(
                new Vector3(Mathf.Ceil(x / CellSize) * CellSize + CellSize, GridOrigin.y, GridOrigin.z - GridSize + CellSize),
                new Vector3(Mathf.Ceil(x / CellSize) * CellSize + CellSize, GridOrigin.y, GridOrigin.z + GridSize + CellSize)
            );
        }
    }

    /// <summary> Attempts to spawn and place an instance of the currently selected tile at the current cursor world position </summary>
    private void AttemptTilePlacement()
    {
        if (IsPlacingTile())
        {
            // Spawn a new instance of the currently selected tile in the palette at the click location's world pos
            GameObject spawnedTile = PrefabUtility.InstantiatePrefab(tilePalette.ElementAt(currentTileIndex).Value.TilePrefab) as GameObject;
            spawnedTile.transform.rotation = cursorTile.transform.rotation;
            spawnedTile.transform.position = cursorTileChild.transform.position;
            spawnedTile.tag = "GridmapTile";
        }
    }

    /// <summary> Creates a new cursor tile instance from the currently selected tile </summary>
    private void CreateNewCursorTile()
    {
        if (currentTileIndex >= 0 && currentTileIndex < tilePalette.Count)
        {
            // Create empty gameobject as a parent in accordance with https://www.xaviscript.com/en/rotate-object-around-center-unity/
            // This guarantees that the center of the tile will be the pivot point, which helps with rotation later on
            if (cursorTile != null)
            {
                DestroyImmediate(cursorTile);
            }

            cursorTile = GameObject.CreatePrimitive(PrimitiveType.Cube);//new GameObject();
            cursorTile.transform.localScale = new Vector3(CellSize, 1.0f, CellSize);
            cursorTile.GetComponent<MeshRenderer>().enabled = false;

            if (cursorTileChild != null)
            {
                DestroyImmediate(cursorTileChild);
            }

            cursorTileChild = PrefabUtility.InstantiatePrefab(tilePalette.ElementAt(currentTileIndex).Value.TilePrefab) as GameObject;
            cursorTileChild.name = "CursorTileChild";
            cursorTileChild.transform.rotation = Quaternion.identity;
            Vector3 objCenter = cursorTileChild.GetComponentInChildren<MeshRenderer>().bounds.center;
            cursorTile.transform.position = new Vector3(objCenter.x, GridOrigin.y, objCenter.z);
            cursorTileChild.transform.parent = cursorTile.transform;
            cursorTileChild.transform.localPosition = Vector3.zero;
            cursorTile.GetComponent<Transform>().hideFlags = HideFlags.HideInHierarchy;
        }
    }

    /// <summary> Adds one or more selected prefabs to the tile palette </summary>
    private void AddTilesToPalette()
    {
        // Loop through the selected game objects that the player wishes to add to the palette.
        foreach (GameObject tileObject in Selection.gameObjects)
        {
            // If the game object is not already present in the tile palette
            if (!tilePalette.ContainsKey(tileObject.name))
            {
                // If the game object is a prefab
                if (tileObject.gameObject.scene.name == null)
                {
                    // If the game object has a collider
                    if (tileObject.GetComponent<Collider>() != null)
                    {
                        // Create a new tile
                        TileInfo newTileInfo = GenerateNewTileInfo(tileObject);

                        int nextTilePathIndex = gridSettingsSerial.FindProperty("tilePaths").arraySize;
                        gridSettingsSerial.FindProperty("tilePaths").InsertArrayElementAtIndex(nextTilePathIndex);
                        gridSettingsSerial.FindProperty("tilePaths").GetArrayElementAtIndex(nextTilePathIndex).stringValue = newTileInfo.TilePath;

                        tilePalette.Add(tileObject.name, newTileInfo);

                        LogGridmap(tileObject.name + " added to tile palette.");
                    }
                    else
                    {
                        LogGridmap(tileObject.name + " skipped due to not having a collider.");
                    }
                }
                else
                {
                    LogGridmap(tileObject.name + " skipped due to not being a prefab.");
                }
            }
            else
            {
                LogGridmap(tileObject.name + " skipped due to already being present.");
            }
        }
    }

    /// <summary> Creates and populates a new TileInfo instance for a given prefab </summary>
    /// <param name="_tilePrefab">A prefab instance</param>
    /// <returns>A TileInfo instance representing the newly created tile</returns>
    private static TileInfo GenerateNewTileInfo(GameObject _tilePrefab)
    {
        return new TileInfo
        {
            TilePrefab = _tilePrefab,
            TileGUIContent = new GUIContent(_tilePrefab.name, AssetPreview.GetAssetPreview(_tilePrefab), _tilePrefab.name),
            TilePath = _tilePrefab.name 
        };
    }

    /// <summary> Removes the currently selected tile from the tile palette </summary>
    private void RemoveSelectedTileFromPalette()
    {
        // If a valid tile is selected
        if (currentTileIndex >= 0)
        {
            // Remove it from the palette
            tilePalette.Remove(tilePalette.ElementAt(currentTileIndex).Key);
        }

        UpdateTilePaths();

        // Reset the currently selected tile
        currentTileIndex = 0;
    }

    /// <summary> Updates the array of saved tile filepaths contained within the gridmap saved settings </summary>
    private void UpdateTilePaths()
    {
        // Remove any existing saved tile paths
        gridSettingsSerial.FindProperty("tilePaths").ClearArray();

        // Add the path for each saved tile path to the saved grid settings
        foreach (KeyValuePair<string, TileInfo> tile in tilePalette)
        {
            int nextArrayElement = gridSettingsSerial.FindProperty("tilePaths").arraySize;
            gridSettingsSerial.FindProperty("tilePaths").InsertArrayElementAtIndex(nextArrayElement);
            gridSettingsSerial.FindProperty("tilePaths").GetArrayElementAtIndex(nextArrayElement).stringValue = tile.Value.TilePath;
        }
    }

    /// <summary> Calculates the new world pos of the cursor based on the given screen pos of the cursor </summary>
    /// <param name="_currentEvent">A reference to the event that is currently being processed by OnToolGUI</param>
    private void UpdateCursorWorldPos(Event _currentEvent)
    {
        Ray cursorRay = Camera.current.ScreenPointToRay(new Vector3(_currentEvent.mousePosition.x, -_currentEvent.mousePosition.y + Camera.current.pixelHeight));
        if (gridPlane.Raycast(cursorRay, out float distance))
        {
            if (cursorTile != null)
            {
                cursorWorldPos = cursorRay.GetPoint(distance);
            }
        }          
    }

    /// <summary> Moves the world position of the cursor to the position of the closest grid cell </summary>
    private void SnapTileCursorToGridCell()
    {
        if (cursorTile != null)
        {
            Vector3 pos = cursorWorldPos;
            pos -= GridOrigin;

            float xRatio = Mathf.Floor(pos.x / CellSize);
            float zRatio = Mathf.Floor(pos.z / CellSize);

            selectedCellPos = new Vector3(
                Mathf.Clamp(xRatio * CellSize, -(GridScale * CellSize) + CellSize, GridScale * CellSize) + (CellSize * 0.5f),
                0.0f,
                Mathf.Clamp(zRatio * CellSize, -(GridScale * CellSize) + CellSize, GridScale * CellSize) + (CellSize * 0.5f)
            );

            selectedCellPos += GridOrigin;

            cursorTile.transform.position = selectedCellPos;
        }
    }

    /// <summary> Performs a series of checks to determine whether a given input is intended to place a tile instance </summary>
    /// <returns> True if the user is considered to be making a valid attempt to place a tile, and false if they are not</returns>
    private bool IsPlacingTile()
    {
        return cursorTile != null && IsCursorWithinGridBounds();
    }

    /// <summary> Checks whether the world position of the mouse cursor is within the bounds of the grid </summary>
    /// <returns>
    /// True if the cursor world position is within the bounds of the grid or false if it is not
    /// </returns>
    private bool IsCursorWithinGridBounds()
    {
        return (cursorWorldPos.x > (GridOrigin.x - GridSize) &&
                cursorWorldPos.x < (GridOrigin.x + GridSize) &&
                cursorWorldPos.z > (GridOrigin.z - GridSize) &&
                cursorWorldPos.z < (GridOrigin.z + GridSize));
    }

    /// <summary> Removes any placed tile instances that overlap with the cursor tile </summary>
    private void AttemptTileDeletion(Event _currentEvent)
    {
        RaycastHit hitInfo = new RaycastHit();
        CheckForRayTileCollision(ref _currentEvent, ref hitInfo);

        if (hitInfo.collider != null)
        {
            if (hitInfo.collider.gameObject != null)
            {
                if (hitInfo.collider.gameObject.tag == "GridmapTile")
                {
                    DestroyImmediate(hitInfo.collider.gameObject);
                }
            }
        }
    }

    /// <summary> Checks to see if a ray projected from the screen cursor intercepts with a tile placed on the grid </summary>
    /// <param name="_currentEvent">A reference to the event that is currently being processed by OnGUI</param>
    /// <param name="_hit">A reference to a RaycastHit object that is used to store the result of the collision test</param>
    private void CheckForRayTileCollision(ref Event _currentEvent, ref RaycastHit _hit)
    {
        Ray cursorRay = Camera.current.ScreenPointToRay(new Vector3(_currentEvent.mousePosition.x, -_currentEvent.mousePosition.y + Camera.current.pixelHeight));

        Debug.Log("Plane point is: " + gridPlane.ClosestPointOnPlane(Vector3.zero));

        if (gridPlane.Raycast(cursorRay, out float distance))
        {
            if (cursorTile != null)
            {
                cursorWorldPos = cursorRay.GetPoint(distance);
            }

            Physics.Raycast(cursorRay, out _hit);
        }
    }

    /// <summary> Checks to see if any placed tile instances overlap with the cursor tile </summary>
    /// <returns> An array containing any collider instances that overlap with the cursor tile </returns>
    private Collider[] CheckForCursorTileOverlap()
    {
        return Physics.OverlapBox(cursorWorldPos, new Vector3(CellSize * 0.45f, 0.5f, CellSize * 0.45f));
    }

    /// <summary> Logs a message to the console with the Unity Gridmap prefix </summary>
    /// <param name="_logMessage">The message to be logged to the console</param>
    private void LogGridmap(string _logMessage)
    {
        Debug.Log("Unity Gridmap || " + _logMessage);
    }
}