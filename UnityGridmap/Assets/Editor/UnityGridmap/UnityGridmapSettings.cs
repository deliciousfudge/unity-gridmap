﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

// Settings class used for storing persistent data related to the gridmap
[System.Serializable]
class UnityGridmapSettings : ScriptableObject
{
    [SerializeField]
    public float cellSize = 1.0f;

    [SerializeField]
    public int gridScale = 10;

    [SerializeField]
    public Vector3 gridOrigin = Vector3.zero;

    [SerializeField]
    public List<string> tilePaths = null;
}
